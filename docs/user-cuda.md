---
id: user-cuda
title: CUDA
sidebar_label: CUDA
---

If configured by a server admin, you may have the ability to switch CUDA versions instantly. When switching CUDA versions, you may also get a python machine learning software stack (tensorflow, keras, jupyter notebook). 

A default software stack may also have been configured by your administrator. If this is the case, you can make new container, `gpu get`, `import tensorflow`, and run your code instantly.

> If at all possible, please try to prevent installing CUDA to your container. By sharing these libraries, we can significantly reduce disk usage. If you need a different version, ask a server admin.

## Versions

Run the following command to list available CUDA versions:

`ls /opt/cuda/`

Run the following version to show the exact CUDA patch version:

`cat /opt/cuda/<version>/version.txt`

Run the following command to show the associated library versions (if any):  

`cat /opt/cuda/<version>/libraries.txt`

## Switching versions

After identifying the version you need, run `cuda <version>` and logout. When you login, you should have access to requested version. Run `nvcc --version` to verify your your CUDA version.

> If you have globally installed python libraries, they may be overwritten when you switch cuda versions. You should use virtual environments with the `--system-site-packages` option.