---
id: design-motivation
title: Design Motivation
sidebar_label: Motivation
---

In the beginning, we managed several physical Ubuntu servers. To enable users to quickly add and remove software, everyone had root access. This lead to the servers being broken very often as people would change global library versions. 

After examining some other tools, we determined that nothing existed that would solve the problems we faced. We began to develop a set of tools to solve our problems.

## What We Do

- Provide users an easy self service way to create containers
- Give the user root access to the container and allow them to modify it however they want
- Manage resources not scheduled by the kernel (GPUs)
- Limit individual container disk usage
- Allow users to run persistent services
- Allow users to build and test containers to deploy on production systems
- Provide access controls for NFS shares

## What We Don't Do

- Multi-server jobs/running
    - Use kubernetes, slurm, etc
- Manage CPU/memory resources
    - Containers compete for all resources