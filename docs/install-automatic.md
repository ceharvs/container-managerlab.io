---
id: install-automatic
title: "Automatic Install"
sidebar_label: "Automatic Install"
---

We provide some scripts to install all our tools easily. You can find them [here](https://gitlab.com/container-manager/extras/tree/master/host-install). You can use the `run-parts` command to execute them sequentially. We also compile the scripts to a single file so that you can install without cloning the whole repo:

```
curl https://container-manager.gitlab.io/extras/install.sh | sudo sh
```

This installer will use the `dir` or `lvm` backend from lxd. If you configured your disk per [these](install-manual.md#ubuntu-install) instructions, the installer should use lvm automatically.

## What's happening?

#### 10-update
- update Ubuntu

#### 20-nvidia
- install the nvidia driver from the cuda repo (without cuda)
- install the nvidia-contaienr-runtime

#### 30-configure-lxd
- install lxd from apt
    - you could switch that to snap install if you want newer features (nested btrfs subvolume)
- decide wether to use `dir` or `lvm`
- set the network subnet
- configure the managed-container profile

#### 40-import-ubuntu-container
- import the ubuntu/18.04 image from [gitlab](https://gitlab.com/container-manager/images)
    - you must use our custom images (we inject ssh keys and other misc configs)

#### 50-install-container-manager
- configure our [repo](https://gitlab.com/container-manager/repo)
- install `ssh-portal` and `gpu-manager`


## Other notes

You could set the `SSHPORTAL_PORT` in `/etc/environment` if you want to change the port. I like to switch the ssh port to 22.

You'll need to configure a ldap server if you want to do authentication. Perform these steps:

```
sqlite3 /home/gpu/ssh-portal.db
sqlite> insert into configs (`key`, `value`) values ('ldap_host', 'ldap.example.com');
sqlite> insert into configs (`key`, `value`) values ('ldap_base', 'ou=Users,dc=example,dc=com');

# optional if you are allowed to anonymously bind
sqlite> insert into configs (`key`, `value`) values ('ldap_bind_dn', 'CN=serviceaccount,OU=Users,dc=example,dc=com');
sqlite> insert into configs (`key`, `value`) values ('ldap_bind_password', 'PASSWORD');

```

If you don't want to configure ldap, you could temporarily set `SSHPORTAL_TESTING=true` in /etc/environment. When `SSHPORTAL_TESTING` is set, any username with password `test` is accepted. Disable it after you have authenticated.