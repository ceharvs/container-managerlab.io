---
id: admin-project
title: Project
sidebar_label: Project
---

This page describes operations available to project admins. Please note that the project itself must be configured by a server/SSH admin.

To access the admin interface, SSH to `admin@gpu.mitre.org`.

In the admin interface, we use the term `group` rather than `project`. A group is a combination of any number of users, remotes (containers), hosts, and tags.

## Adding and Removing Users

You'll need to know the exact name of the project and the user. You can use the `user ls` and the `project ls` command to find both of those items.

Run the following command to add a user to a group:

`group update --adduser <user-name> <project-name>`

Run the following command to remove a user from a group:

`group update --removeuser <user-name> <project-name>`

> When a user is removed from a group, their existing mount points are not removed. The user (or SSH admin) must run `project detach <project-name>`

Alternatively, a project could be configured to be accessible to all users. Run the following command to allow access by all users:

`group update --setallusers <project-name>`

The previous setting can be unset via the following command:

`group update --unsetallusers <project-name>`

## Adding and Removing Admins

Run the following command to grant admin access:

`group update --addadmin <user-name> <project-name>`

Run the following commmand to remove admin access:

`group update --removeadmin <user-name> <project-name>`

## Host Access

If a host has been access restricted, adding a user to the project group will grant the user access to the host. Only an SSH admin can add restrict host access and add hosts to a group.

## readonly

If the `readonly` tag is set on the project, normal users will only have read access. On `project attach`, a project admin will be granted write access. If a user is granted admin after they initially attached the project to their container, they need to rerun `project attach`. A user will retain their write access even if their admin is removed (until the next time they attach/detach).