---
id: admin-roles
title: Admin Roles
sidebar_label: Admin Roles
---

This page describes role of the three different types of administrators.

## Project
- Adds and removes users from projects
- Adds and removes other project admins
- Grants access to project specific servers

## SSH
- Can perform any action on any container in the containers shell
    - `rm`, `migrate`, etc.
- Can perform any action in the admin shell
    - `user create`, `user rm`, etc.

## Server
- Configures NFS mounts
- Has root access to all servers
- Performs host updates (kernel, drivers, etc)
- Monitors disk usage and failures