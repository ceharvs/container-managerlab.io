---
id: user-quick-start
title: Quick Start
sidebar_label: Quick Start
---

Follow these steps to quickly get access to a container. You need to know the hostname of your server. In this example, we use `gpu.mitre.org` as a placeholder.

1. Generate SSH keys and ensure they are being provided by your client
2. SSH to `register@gpu.mitre.org`
    - Enter your LDAP Credentials
3. SSH to `containers@gpu.mitre.org`
4. At this shell, type `create --image ubuntu/18.04 test`
5. Exit the containers shell
6. SSH to `test@gpu.mitre.org`

### What's Happening?

We hijack the username parameter to decide where to route your connection. If the username is the name of a internal shell (`register`, `containers`, `admin`), then you'll be dropped into that shell. 

> When you interact with these shells, you're actually interacting with custom programs (rather than a real UNIX shell)

If the username doesn't match a shell name, we assume it's a container name. If a container exists with that name, your connection will be routed to the SSH daemon running on the container.

We presented about this system and other technology used at MITRE during NVIDIA GTC 2019 ([link](https://on-demand.gputechconf.com/gtc/2019/video/_/S9958/)). Skip to 17:00 for specifics on this project.

To learn more about shells, follow the link below.
