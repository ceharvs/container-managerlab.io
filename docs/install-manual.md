---
id: install-manual
title: "Manual Install"
sidebar_label: "Manual Install"
---

### This document is out of date. We're keeping it for now as a reference.

This document describes how to install the entire system without any automation. In this guide we use LVM, but you could also run the system on ZFS or btrfs.

This guide is primarily designed for a single server deployment. If you want to run multiple linked servers, you'll also need an instance of mysql.

## Ubuntu Download

You'll need to download the Ubuntu Server alternative installer [link](http://cdimage.ubuntu.com/releases/18.04.1/release/ubuntu-18.04.1-server-amd64.iso). We need the alternative installer because it is able to configure LVM partitioning.

## Ubuntu Install

Most default options are acceptable. However, we need to use LVM for disk partitioning. Here's what the partition layout should look like:

```shell
+--------------------------+
|          LVM PV          |
| +----------------------+ |
| |        LVM VG        | |
| | +------------------+ | |
| | |      LVM LV      | | |
| | |                  | | |
| | +------------------+ | |
| |                      | |
| |                      | |
| |                      | |
| |                      | |
| |                      | |
| |                      | |
| |                      | |
| |                      | |
| +----------------------+ |
+--------------------------+
```

Name the physical volume (PV) `pv` and the volume group (VG) `vg`. Create one logical volume (LV) on `vg`, name it `root`, and mount it as `/`. The root LV should be 20 - 50 GB. Leave the rest of the disk empty.

> Consider creating the LVM PV on top of a software raid (md) if you have not configured hardware RAID.

Run the installer to completion and reboot the system.

## Install Nvidia Drivers

Skip this step if you don't have Nvidia GPUs.

Run the following commands to install the latest drivers:

```shell
apt install -y software-properties-common curl

curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | \
  sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | \
  sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
sudo apt-get update
apt install -y nvidia-container-runtime

add-apt-repository -y ppa:graphics-drivers/ppa
apt install -y nvidia-headless-410 nvidia-utils-410 libnvidia-cfg1-410

sudo bash -c "echo blacklist nouveau > /etc/modprobe.d/blacklist-nvidia-nouveau.conf"
sudo bash -c "echo options nouveau modeset=0 >> /etc/modprobe.d/blacklist-nvidia-nouveau.conf"
sudo update-initramfs -u
```

Reboot the server.

## LVM Thinpool Creation

Install the thinpool tools by running: `apt install -y thin-provisioning-tools`.

Create the thinpool by running: `lvcreate -L <SIZE> -T vg -n pool`

> Leave extra space at the end of the pool. This allows recovery from failure if users fill up the pool. Recovery becomes **much** more difficult if you don't leave extra space at the end.

## LXD Configuration

First we purge the default version of lxd. Run `apt purge lxd*`.

Next we install the latest version of lxd with `snap install lxd`. Logout and login to update your `$PATH`.

Then we use the `lxd preseed --init` command to set the initial config:

```shell
cat << EOF | lxd init --preseed
config:
  core.https_address: '[::]:8443'
  core.trust_password: MY_TRUST_PASSWORD
  images.auto_update_interval: 0
  images.remote_cache_expiry: 0
cluster: null
networks:
- config:
    ipv4.address: 10.199.0.1/16
    ipv4.nat: "true"
    ipv6.address: none
  description: ""
  managed: false
  name: lxdbr0
  type: ""
storage_pools:
- config:
    source: vg
    volume.size: 50GB
    lvm.thinpool_name: pool
  description: ""
  name: default
  driver: lvm
profiles:
- config:
    linux.kernel_modules: overlay
  description: ""
  devices:
    eth0:
      name: eth0
      nictype: bridged
      parent: lxdbr0
      type: nic
    root:
      path: /
      pool: default
      type: disk
  name: default
- config:
    nvidia.runtime: true
    security.nesting: true
  description: ""
  name: managed-container
EOF
```

> Set the `core.proxy_http`, `core.proxy_https`, and `core.proxy_ignore_hosts` in the main config section if behind a proxy

> If you are installing on a single server, remove the `config.core.https_address` and `config.core.trust_password` options. Otherwise, generate a random secret and replace `MY_TRUST_PASSWORD` before running the command.

> Consider adjusting the `storage_pools[0].config.volume.size` option. This is the maximum size each container and `/local/` volume.

> Remove the `nvidia.runtime` option from `profiles[1].config` if this is not a GPU server.

## ssh-portal Configuration

Set these options in `/etc/environment` to configure ssh-portal:

- `SSHPORTAL_NET`: the cidr ipv4 network for containers
    - This should match the `networks[0].config.ipv4.address` setting from the lxd config
- `SSHPORTAL_LXCCONFIG`: this points to a directory that contains the `lxc` configs
    - Example: `SSHPORTAL_LXCCONFIG=/root/snap/lxd/9600/.config/lxc/config.yml`
- `SSHPORTAL_LDAPBase`: the base OU of the LDAP search
    - Example: `SSHPORTAL_LDAPBase='ou=People,o=mitre.org'`
- `SSHPORTAL_LDAPHost`: the hostname of the LDAP server
    - Example: `SSHPORTAL_LDAPHost='ldap-user.mitre.org'`
- `SSHPORTAL_PORT`: sets the port
    - This defaults to `2222`. You probably want to set it to `22`.
    - If you set it to `22`, you need edit `/etc/ssh/sshd_config`
- `SSHPORTAL_SQLDIALECT`: the SQL dialect
  - `sqlite3` (default), `mysql`
- `SSHPORTAL_SQLCONFIGSTRING`: string to pass to the SQL engine
  - For `sqlite3`, this is the path of the database
  - For `mysql`, the [DSN](https://github.com/go-sql-driver/mysql#dsn-data-source-name) of the database
- `SSHPORTAL_ENFORCEADMINTOKEN`: set this option to require all admins to use hardware tokens
  - You should not set this option until you have configured an administrative user.
- `LXD_SOCKET`: lxd's management socket
    - `LXD_SOCKET=/var/snap/lxd/common/lxd/unix.socket`

## ssh-portal Installation

If you have the `.deb` for ssh-portal, install it with `dpkg -i ssh-portal.deb`. The service will automatically start.

If you want to build from source, install `golang-1.11` ([instructions](https://github.com/golang/go/wiki/Ubuntu)). `cd` into the source directory, switch to the containers branch, then run `go build`. You could use `nfpm` to build a `.deb` or manually run `./ssh-portal` yourself.

Now you should be able to SSH to and use `register/containers/admin`.

## Download the Ubuntu 18.04 image

`lxc image copy --alias ubuntu18.04 ubuntu:18.04 local:`

> All required steps are complete. The next steps are optional.

## Deploy Volume Configuration

You need to create a shared mount point to deploy the `gpu` and/or the `cuda` commands to containers. During `cloud-init`, these tools are symlinked into `/usr/bin/`.

Run the following command to create the mount point:

`mkdir -p /opt/deploy`

Run the following command to mount `/opt/deploy` on the `managed-container` profile.

`lxc profile device add managed-container deploy disk path=/opt/deploy source=/opt/deploy`

Items you place in `/opt/deploy` will now be visible to all containers (read only).

The `cuda.sh` script is in the `container-manager-extras` repo.

## Container Configuration

The `container-manager-extras` repository contains tools to build the `cloud-init` configuration for containers. `cloud-init` will only be applied on ubuntu containers.

You could add specific commands to `./configs/install.sh` and `cloud-init` style configurations to `./configs/config.yml`. We configure our `http_proxy` in `install.sh` and our certificates in `config.yml` ([reference](https://cloudinit.readthedocs.io/en/latest/topics/examples.html#configure-an-instances-trusted-ca-certificates)).

> The `users`, `system`, and `runcmd` directives in `config.yml` will be ignored because of the configuration that `ssh-portal` applies.

Once the configuration is complete, run `./profile.sh`. This command will load the configurations into the `managed-container` profile.

## gpu-manager Server Configuration

Set these options in `/etc/environment` to configure `gpu-manager`:

- `STATS_SERVER`: the http address of the stats server
- `SLACK_API_KEY`: the Slack API token
  - Starts with `xoxp-`
  - Enables the `gpu` slack integration

## gpu-manager Server Installation

If you have the `.deb` for gpu-manager, install it with `dpkg -i gpu-manager.deb`. The service will automatically be started.

This program is built the same way as `ssh-portal`.

## gpu-manager Client Installation

Run `go build` in the `gpu-manager/cli/` directory. Copy the `cli` binary to `/opt/deploy/gpu`.

> You could install the cli via a `.deb`, but you can lose the ability to instantly update it in all containers.

## CUDA Volume Creation

Run the following command to create the volume:

`lxc storage volume create default deploy`

Run the following command to attach volume to the `managed-container` profile.

`lxc profile device add managed-container cuda disk path=/opt/cuda source=cuda pool=default readonly=true`

## CUDA Library Configuration

Create a clean container. Mount the `cuda` volume into the container with the following command:

`lxc device add <container-name> cuda disk path=/opt/cuda source=cuda pool=default`

> We remove the `readonly` option and apply the configuration to a container rather than a profile. In lxd, container specific configurations override profile configurations.

Inside the container, do the following:

- Download the needed CUDA libraries (in the .run) format
- Execute the .run, pointing the installer to `/opt/cuda/<version>`
  - (optional) install the samples to `/opt/cuda/<version>/samples`

Document the exact version in `/opt/cuda/<version>/versions.txt`

You can also optionally install compatible python libraries to `/opt/cuda/<version>/python3/`. Use the `--target` option on `pip` to install to this directory.

Document the exact versions in `/opt/cuda/<version>/libraries.txt`