---
id: design-decisions
title: Design Decisions
sidebar_label: Decisions
---

This document describes some design decisions of this project.

## lvm vs btrfs vs ceph

We use the LVM backend for lxd because it's the most stable. We also really care about performance and other backends don't compete in that arena.

Ceph is pretty promising, but we don't have the hardware to support building out a real cluster.

## /opt/cuda

Because we use LVM, we want to make some attempt to reduce disk usage. We do this by giving users the option to use a shared version of cuda in `/opt/cuda`. But they can ultimately do whatever they want.

## lxd clustering

In an ideal world we would use a lxd cluster. However, we have some servers that we can't repartition and have to use the `dir` backend. lxd cluster's require that every server have the same config.

## image-builder using docker vs debootstrap

The primary purpose of this environment is to tinker with software and build docker containers. Because of this, we want our images to be as similar to docker images as possible. We could add new images based on docker images very quickly.

## image-builder templating vs cloud-init

We used cloud-init for the initial prototype, but it consistently resulted in odd behavior. With templates, we can control exactly when something is templated (create, copy, start).

## unique name constrains

Originally, we enforced that all containers must have a unique name. That requirement was relaxed recently. Now the unique name constraint is per user.

## internal container names

If you run `lxc ls`, you'll notice that containers are named things like `c10` and `c60`. We use a generated internal name so that we can have multiple containers with the same host name owned by different users. We can also rename containers without restarting them.

We store the actual hostname in `user.remotename`. This is name is templated into the container via distrobuilder.

## ssh vs web/api

The initial prototype was actually web based. We moved to pure SSH access/admin for simplicity. We have to have an ssh server anyway, so we might as well do everything via it.

Automation is just as easy with via SSH compared to HTTP API.