---
id: user-shells
title: Shells
sidebar_label: Shells
---

This page describes each shell of the user facing system.

## register

The register shell associates your SSH keys with your LDAP account. You can associate multiple keys with one account.

> Your keys will expire if they are not used for 10 days. To reactivate them, you must re-register.

## containers

The containers shell allows you to perform a number of container operations. Type `help` at the shell to see possible operations. This section will provide additional information where relevant.

### create

`create` creates containers. You must provide the `--image` options and the name. 

Images are configured by server admins. If you need additional images, ask a server admin.

You can list available images by running `images`.

The container name must be a valid hostname. If your deployment contains multiple servers, the name of the container must be unique to your user.

After running the create command, you must wait for your container to be provisioned. You may experience errors such as `connection refused` or `ssh handshake failed` until this process is complete.

### forward

`forward` creates port forwards between the container host and your container. After creating a forward, you should be able to access a network service on your container by connecting to the host.

If your deployment contains multiple servers, the host port (lhandport) must be unique among all servers. Forwards will survive migration across multiple servers.

### migrate

`migrate` moves containers between multiple hosts. To move a container from `host1` to `host2`, follow these steps: 

1. SSH to `containers@host2.mitre.org`
2. Type `migrate <container-name>` 

The container will be "pulled" from `host1` to `host2`. All mounts will be removed when the container is migrated.

### project

`project` allows you to attach and detach from shared resources. Project access must be explicitly granted by a project admin. Once granted access to a project, you may attach and detach it to any container.

When you run `project ls`, you'll notice a column called tags. Here is a description of what those tags mean:

- `nfs`: the project contains a nfs share which will be mounted at `/nfs/<project-name>`
- `local`: the project contains a local share which will be mounted at `/local/<project-name>`. This mount resides on the local disk of the host. This should be considered temporary space which may be manually purged by an admin at any time.
- `readonly`: the project mounts are read only (unless you are a group admin)
- `token`: once attached, your container will forever require a hardware token to login. See [Hardware Tokens](user-hardware-tokens) for more details.
- `network`: a new project specific network interface will be added to your container. See [Networking](user-networking) for more details.

> Projects may have complex requirements for access (NDAs, etc). Your project leader should be able to provide you more information.

### share

`share` allows you to share a container with project members. Here's some examples:

`share --project my-project my-container`

In this configuration, usernames are directly routed into the container. The sharing user must `useradd` the appropriate users inside the container (`useradd -ms /bin/bash alice`).

- bob -> `ssh my-container@gpu.mitre.org` -> `bob@my-container`
- alice -> `ssh my-container@gpu.mitre.org` -> `alice@my-container`

`share --project my-project --username root my-container`

In this configuration, usernames are discarded. Every user will access will be routed to the specified user. You must ensure this user exists before sharing a container.

- bob -> `ssh my-container@gpu.mitre.org` -> `root@my-container`
- alice -> `ssh my-container@gpu.mitre.org` -> `root@my-container`