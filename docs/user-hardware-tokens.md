---
id: user-hardware-tokens
title: Hardware Tokens
sidebar_label: Hardware Tokens
---

When you attach a project with the `token` tag set, your container will require you to use a hardware token to authenticate. Hardware tokens may also be required for admin operations. This page describes how this process works.

## Obtain Hardware Token

We support these hardware tokens:

- [yubikey](https://www.yubico.com/product/yubikey-5-nano/)
- [PIV/CAC](https://piv.idmanagement.gov/engineering/ssh/)
- Mac Secure Enclave ([sekey](https://github.com/ntrippar/sekey))

Your organization may have different requirements.

## Configuration

Once you have obtained and configured a token, use the `register` shell to register it. Then talk to an admin in person. They will visually verify the public key of your token and mark it as a hardware token.
