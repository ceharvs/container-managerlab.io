---
id: gitlab-ci
title: Gitlab CI
sidebar_label: Gitlab CI
---

This section describes how to configure your `.gitlab-ci.yml` to use our custom gitlab runner executors.

## Overview

We've developed both a docker and shell executor. Here are some interesting things you can do with our runners:

- Create a new `container-manager` container for every ci job
- Load balance your CI jobs across multiple servers
- Declare how many and which type GPUs you need
    - `gpu get --lock <count>` will be called transparently in the background
- `share` that container with a group
- `attach` project resources

## Variables

When using our runners, you can specify variables to define some behavior. Check out the examples below for more info.

- `CI_CM_GPU_REGEX`
    - A golang regex to match GPU names
        - `V100`
        - `P100`
        - `V100|P100`
        - `32GB`
    - Defaults to all GPUs
- `CI_CM_GPU_COUNT`
    - Number of GPUs to request
    - Defaults to 0
- `CI_CM_PROJECT_TOKEN`
    - Token to allow you to attach to projects. Created by admin. Anyone who has this token can attach to the projects it represents.
- [shell] `CI_CM_SHARE_PROJECT`:
    - Name of the project to `share` this container with
- [shell] `CI_CM_ACTIONS`
    - Comma separated actions to take during the job
        - `create`: create a new container
        - `rm-before`: remove the container before the job
        - `rm-after`: remove the container after the job
        - `share`: share the container to group from `CI_CM_SHARE_PROJECT`
        - `attach`: attach projects from `CI_CM_PROJECT_TOKEN`

You should configure `CI_CM_PROJECT_TOKEN` and `CI_CM_SHARE_PROJECT` under `Settings -> CI/CD -> Variables`. It works really well to configure this at the gitlab group/subgroup level, because all child repos will inherit the configuration.

## Docker Executor

The Docker runner is the easiest way to run your CI jobs. When developing with this runner, you maintain 95% compatibility with gitlab.com or the docker-machine executor.

A basic `.gitlab-ci.yml` would look like this:
```yaml
image: ubuntu

test:
  script:
    - echo "hello world"
```

Here's a simple GPU example:
```yaml
image: ubuntu

test:
  variables:
    NVIDIA_VISIBLE_DEVICES: all
    CI_CM_GPU_COUNT: 1
    CI_CM_GPU_REGEX: P100
  script:
    - nvidia-smi
```

Testing your program across multiple GPU architectures (parallel):
```yaml
image: ubuntu

test-p100:
  stage: test
  variables:
    NVIDIA_VISIBLE_DEVICES: all
    CI_CM_GPU_COUNT: 1
    CI_CM_GPU_REGEX: P100
  script:
    - nvidia-smi

test-v100:
  stage: test
  variables:
    NVIDIA_VISIBLE_DEVICES: all
    CI_CM_GPU_COUNT: 1
    CI_CM_GPU_REGEX: V100
  script:
    - nvidia-smi
```

## Shell Executor

When performing more complicated jobs, it becomes much easier to use the shell executor. With this executor, we can persist our container across multiple jobs.

This executor creates a container with the `ubuntu/18.04/docker` image. It has `docker` and `docker-compose` installed by default.

Here's an example with build and pushes a docker container:
```yaml
test:
  variables:
    CI_CM_ACTIONS: create,rm-after
  script:
    - docker login -e $DOCKER_EMAIL -u $DOCKER_USER -p $DOCKER_PASS
    - docker build -t $IMAGE_NAME
    - docker tag test $DOCKER_USER/$IMAGE_NAME
    - docker push $DOCKER_USER/$IMAGE_NAME
  tags: shell
```

Here's an example which creates a gitlab review app. This will persist until you press the stop button in the gitlab ui or merge the merge request.
```yaml
stages:
 - create
 - up
 - stop

create_review:
  stage: create
  variables:
    CI_CM_ACTIONS: create,share,attach
  tags:
    - shell
  script:
    - echo "created"
  environment:
    name: review/$CI_PROJECT_PATH_SLUG-$CI_BUILD_REF_SLUG
    on_stop: stop_review

install:
  stage: up
  tags:
    - shell
  script:
    - docker-compose up -d

stop_review:
  stage: stop
  variables:
    GIT_STRATEGY: none
    CI_CM_ACTIONS: rm-after
  tags:
    - shell
  script:
    - echo "Remove review app"
  when: manual
  environment:
    name: review/$CI_PROJECT_PATH_SLUG-$CI_BUILD_REF_SLUG
    action: stop
```

> You shouldn't put any real commands in the script section of the create stage. If creation is successful but your script fails, you'll leak a container.

Setting `only: merge_request` in the above example allow you to create a review app every time someone opens a merge request. While this is very useful, you need to consider that if there are additional commits pushed to the MR branch, all the jobs will run again. 

Consider setting `CI_CM_ACTIONS:rm-before,create` in the `create_review` job to delete any existing container before executing subsequent jobs. Or design your jobs to handle being run multiple times.