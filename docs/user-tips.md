---
id: user-tips
title: Tips
sidebar_label: Tips
---

This section contains additional information which may be helpful to users.

## Networking

Containers can access our internal network, but (by default) cannot be directly accessed. You could add a port forward in the containers shell, however there would be no access control. We recommend using SSH port forwarding to access services on your container.

When SSHing, use the `-L` option to forward a port on your local system to your container. For example:

`ssh -L 8888:localhost:80 mycontainer@gpu.mitre.org`

| Component | Description |
| --- | --- |
| 8888 | Source port on your local machine |
| localhost | Target host where the connection is forwarded |
| 80 | Target port |
| mycontainer | The container name |
| gpu.mitre.org | The host where the container is running |

Now if you connect to `localhost:8888` on your local machine, it's like connecting to `localhost:80` inside the container.

If you want to communicate between containers, you should use [project networking](user-networking).

## xpra

While X11 forwarding works, we recommend using a program called `xpra`

`xpra` renders a remote x11 session and streams the video to your local X server via an SSH TCP tunnel. You can use it to attach and detach from remote X11 sessions.

You need to install `xpra` on both your local computer and your container.

### Installation in your container

On Ubuntu, you can do this by running `sudo apt install -y xpra xterm`.

### Installation on your local Mac

Locally, you can install xpra for Mac using [homebrew](https://formulae.brew.sh/cask/xpra) (installation instructions for homebrew on their homepage [here](https://brew.sh/)).

### Installation on your local Windows PC

Clone and [build xpra from source](https://github.com/Xpra-org/xpra#build-from-source).

### Using xpra

On your local computer you can then run `xpra start ssh:mycontainer@gpu.mitre.org --start=/usr/bin/xterm`. After a delay, you should see an xterm window pop up on your display.

Alternatively, you could run `xpra start --systemd-run=no` on your container and `xpra attach ssh:mycontainer@gpu.mitre.org` on your local computer.

If you run into errors, try the following: `apt purge xserver-xorg-legacy`.

If using Ubuntu 16.04, you may need to install the latest version of xpra from their website.

## docker

We only support the Ubuntu 18.04 `docker.io` package. If you use the `ubuntu/18.04/docker` image, docker is already installed/configured/running. Otherwise, perform the following steps:

- `sudo /opt/deploy/docker/configure.sh`
- `sudo apt install docker.io`

> If using GPUs, you must start your docker container after you `gpu get`. If your lease times out, you will have to start your container again.

`configure.sh` performs the following actions
- Installs the [docker-runc-hooks](https://gitlab.com/container-manager/docker-runc-hooks) "runtime"
    - This runtime applies "hooks" from `/etc/docker/hooks.d`
    - These hooks configure nvidia devices, proxies, and TLS certificates
    - You can disable any of them by deleting the relevant files in `/etc/docker/hooks.d`
- Sets the default runtime to `docker-runc-hooks`
    - This means all hooks are applied to all containers during build and run
- Sets the graph-driver to `overlay`
    - `overlay2` only work on the nightly version of docker-ce (as of 5/19)
- Sets the DNS server to 10.199.0.1
    - When running nested containers, you must explicitly set the DNS server
