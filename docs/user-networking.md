---
id: user-networking
title: Networking
sidebar_label: Networking
---

If configured, project networking allows your containers on multiple hosts to communicate via an isolated network.

## Attaching to a network

If a project has a `network` tag, you can connect to that network via the `project attach` command at the `containers` shell. Example:

```shell
ssh containers@gpu.mitre.org
> project attach --project networkdemo mycontainer
```

```shell
ssh mycontainer@gpu.mitre.org
agartner@mycontainer:~$ ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
836: eth0@if837: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 00:16:3e:d6:bc:62 brd ff:ff:ff:ff:ff:ff link-netnsid 0
838: eth1@if834: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 00:16:3e:b8:62:e5 brd ff:ff:ff:ff:ff:ff link-netnsid 0
```

Once you've attached the network, you'll need to `sudo systemctl restart systemd-networkd` or `reboot` to obtain an IP address via DHCP.

You can reach other containers by using `<container-name>.local`. Example:

```shell
agartner@mycontainer:~$ ping mycontainer2.local
PING mycontainer2.local (192.168.101.192) 56(84) bytes of data.
64 bytes from mycontainer2.local (192.168.101.192): icmp_seq=1 ttl=64 time=0.437 ms
```

> If you plan on attaching multiple project networks, consider prefixing container names to avoid naming conflicts

## Docker

We use [mDNS](https://en.wikipedia.org/wiki/Multicast_DNS) to resolve container names (mycontainer.local). This lookup is performed by the `systemd-resolved` service. By default, docker won't be able to communicate with this service. You can workaround this issue by using `host` networking and configuring a dns server. Example:

```shell
docker run -it --rm --net=host --dns=127.0.0.53 alpine
```

If you are using `docker-compose`, you could use a [`docker-compose.override.yml`](https://docs.docker.com/compose/extends) to configure these settings:

```yaml
app:
    net: host
    dns: 127.0.0.53
```