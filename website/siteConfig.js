/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const siteConfig = {
  title: 'container-manager', // Title for your website.
  tagline: 'Tools to easily manage lxd containers',
  baseUrl: '/', // Base URL for your project */
  organizationName: '',
  projectName: '',
  cname: '',
  url: '',

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    {doc: 'user-quick-start', label: 'Docs'},
  ],

  /* Colors for website */
  colors: {
    primaryColor: '#2E8555',
    secondaryColor: '#205C3B',
  },

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} The MITRE Corporation`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  // No .html extensions for paths.
  cleanUrl: true,
  enableUpdateBy: true,
  enableUpdateTime: true,
};

module.exports = siteConfig;
